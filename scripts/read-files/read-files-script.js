/**
 * Function to read all jsons in src\data folder
 * @param {Path} dirname path to src\data
 * @param {Function} onFileContent given function
 * @param {Function} onError given error function
 */

const fs = require('fs');
module.exports = function readFiles(dirname, onFileContent, onError) {
  fs.readdir(dirname, (err, filenames) => {
    if (err) {
      onError(err);
      return;
    }
    // Foreach file read it and call given function onFileContent
    filenames.forEach((filename) => {
      fs.readFile(dirname + filename, 'utf-8', (readFileErr, content) => {
        if (readFileErr) {
          onError(readFileErr);
          return;
        }
        onFileContent(dirname, filename, content);
      });
    });
  });
}
